-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 12, 2015 at 07:06 AM
-- Server version: 5.5.16
-- PHP Version: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `fts_accounting`
--
CREATE DATABASE `fts_accounting` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `fts_accounting`;

-- --------------------------------------------------------

--
-- Table structure for table `expense`
--

CREATE TABLE IF NOT EXISTS `expense` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `expense_category_id` int(11) NOT NULL,
  `receipt_number` varchar(30) NOT NULL,
  `date` date NOT NULL,
  `description` varchar(80) NOT NULL,
  `amount` decimal(9,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `expense`
--

INSERT INTO `expense` (`id`, `user_id`, `expense_category_id`, `receipt_number`, `date`, `description`, `amount`) VALUES
(1, 0, 0, '00001', '2015-03-02', 'Water Bill for the month of February', 2000.00),
(2, 0, 4, '000011', '2015-02-04', 'Payroll for the month of January', 800.00),
(4, 0, 1, '72510001', '2015-02-11', 'Groceries Food drinks and pizza', 1450.00),
(5, 0, 1, '1028', '2015-01-09', 'Banana Cue', 35.00);

-- --------------------------------------------------------

--
-- Table structure for table `expense_category`
--

CREATE TABLE IF NOT EXISTS `expense_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL,
  `description` varchar(100) NOT NULL,
  `client_name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `expense_category`
--

INSERT INTO `expense_category` (`id`, `name`, `description`, `client_name`) VALUES
(1, 'Food', 'Pizza Fridays', 'Ronn Lamostre'),
(2, 'Water Bill', 'DCWD Water Bill', 'Jennifer Conta'),
(3, 'Electricity', 'Electricity Bill', 'Jessica Day'),
(5, 'Popo''s Stuff', 'Dog food, water, haircut, vet, medicine, etc', 'Elson Solano');

-- --------------------------------------------------------

--
-- Table structure for table `income`
--

CREATE TABLE IF NOT EXISTS `income` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `income_category_id` int(11) NOT NULL,
  `receipt_number` varchar(30) NOT NULL,
  `date` date NOT NULL,
  `description` varchar(80) NOT NULL,
  `amount` decimal(9,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `income`
--

INSERT INTO `income` (`id`, `user_id`, `income_category_id`, `receipt_number`, `date`, `description`, `amount`) VALUES
(1, 0, 1, '120', '2015-03-10', 'Payroll from Alexis Servy', 53276.87),
(2, 0, 1, '1949', '2015-01-06', 'This is some description on whatever it is i am doing', 45000.00),
(3, 0, 2, '27671', '2015-02-08', 'FTS Website Repairs', 35806.00),
(7, 0, 3, '908', '2014-11-30', 'Income for website repairs', 19470.00),
(8, 0, 4, '1312', '2015-07-08', 'Popo at the office', 5000.00);

-- --------------------------------------------------------

--
-- Table structure for table `income_category`
--

CREATE TABLE IF NOT EXISTS `income_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  `description` varchar(255) NOT NULL,
  `client_name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `income_category`
--

INSERT INTO `income_category` (`id`, `name`, `description`, `client_name`) VALUES
(1, 'Advanced Massage Network', 'Advanced Massage Network (AMN) Payroll', 'Alexis Servy'),
(2, 'FuturisTech Studios', 'FuturisTech Studios (FTS) Payroll', 'Elson James Solano'),
(3, 'Mindanao Rural Development Program', ' (MRDP) Geotagging and Geomapping', 'Juan delos Santos-Armano'),
(4, 'Entertainment by Popo', 'Phew!', 'Popo''s Entertained'),
(5, 'Visor Project', 'Web development services', 'Joe Satriani'),
(6, 'Vino For Me', 'Vino For Me Web Dev Services', 'Paul McCartney');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
