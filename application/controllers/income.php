<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Income extends CI_Controller {

	function index() {
		
		$this->load->view('includes/header');
		$this->load->view('index');
		$this->load->view('includes/footer');

	}

	function categories() {
		$data['filename'] = "category/view";

		$this->load->model('incomeCategory_model');
		$data['filedata']['categories'] = $this->incomeCategory_model->get();
		$data['filedata']['page'] = "Income";

		// echo "<pre>";
		// print_r($data['filedata']); exit;

		$this->load->view('includes/header');
		$this->load->view('includes/nav');
		$this->load->view('index', $data);
		$this->load->view('includes/footer');
	}

	function add() {
		$data['filename'] = "income/add";

		$this->load->model('incomeCategory_model');
		$data['filedata']['categories'] = $this->incomeCategory_model->get();

		// echo "<pre>";
		// print_r($data['filedata']); exit;

		$this->load->view('includes/header');
		$this->load->view('includes/nav');
		$this->load->view('index', $data);
		$this->load->view('includes/footer');
	}

	function get_post_data() {
		$data = array(
			'income_category_id' => $this->input->post('income_category_id', TRUE),
			'receipt_number' => $this->input->post('receipt_number', TRUE),
			'date' => $this->input->post('date', TRUE),
			'description' => $this->input->post('description', TRUE),
			'amount' => $this->input->post('amount', TRUE),
		);

		return $data;
	}

	function insert() {
		$submit = $this->input->post('submit', TRUE);
		$items = $this->input->post('items', TRUE);

			// echo "<pre>";
			// print_r($data); exit;

		if ($submit == "Submit") {
			$data = $this->get_post_data();

			$this->load->model('income_model');
			$this->income_model->insert($data);

			$message = "Item is successfully added.";
			$this->session->set_flashdata('message', $message);

			$active = "income";
			$this->session->set_flashdata('active', $active);

			redirect('home');

		} else if ($submit == "Go" && !empty($items)) {
			echo $items;
		} else {
			echo "error";
		}
	}

	function delete() {
		$id = $this->uri->segment(3);

		$this->load->model('income_model');
		$this->income_model->delete($id);

		$message = "Item is successfully deleted.";
		$this->session->set_flashdata('message', $message);

		$active = "income";
		$this->session->set_flashdata('active', $active);

		redirect('home');
	}

	function update() {
		$id = $this->uri->segment(3);

		$data['filename'] = "income/update";

		$this->load->model('incomeCategory_model');
		$data['filedata']['categories'] = $this->incomeCategory_model->get();

		$this->load->model('income_model');
		$query = $this->income_model->get_where($id);
		$data['filedata']['query'] = $query[0];


		$this->load->view('includes/header');
		$this->load->view('includes/nav');
		$this->load->view('index', $data);
		$this->load->view('includes/footer');
	}

	function update_item() {

		$id = $this->input->post('id', TRUE);
		$data = $this->get_post_data();
		
		// print_r($data); exit;

		$this->load->model('income_model');
		$this->income_model->update($id, $data);
		
		$message = "Item is successfully updated.";
		$this->session->set_flashdata('message', $message);

		$active = "income";
		$this->session->set_flashdata('active', $active);

		redirect('home');	
	}
}