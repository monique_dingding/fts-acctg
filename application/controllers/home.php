<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	function index() {
		$data['filename'] = "home";

		$this->load->model('income_model');
		$data['filedata']['income_query'] = $this->income_model->get_join('income_category');

		$this->load->model('expense_model');
		$data['filedata']['expense_query'] = $this->expense_model->get_join('expense_category');

		// echo "<pre>";
		// print_r($data['filedata']['expense_query']); exit;

		$temp = $this->session->flashdata('active');
		if (empty($temp)) {
			$data['filedata']['active'] = "income";
		} else {
			$data['filedata']['active'] = $temp;
		}		

		$this->load->view('includes/header');
		$this->load->view('includes/nav');
		$this->load->view('index', $data);
		$this->load->view('includes/footer');

	}

	function remove_item() {

		if ($this->input->post('submit', TRUE) == "Proceed") {
			$id = $this->input->post('id', TRUE);
			$type = $this->input->post('type', TRUE);

			if ($type == "Income") {
				redirect('income/delete/'.$id);
			} else if ($type == "Expense") {
				redirect('expense/delete/'.$id);
			}
		}

	}

	function remove_category() {
		if ($this->input->post('submit', TRUE) == "Proceed") {
			$id = $this->input->post('id', TRUE);
			$type = $this->input->post('type', TRUE);

			if ($type == "Income") {
				redirect('incomeCategory/delete/'.$id);
			} else if ($type == "Expense") {
				redirect('expenseCategory/delete/'.$id);
			}
		}
	}

	function general_ledger() {
		$data['filename'] = "general_ledger";

		$this->load->model('income_model');
		// $data['filedata']['income_query'] = $this->income_model->get_join('income_category');
		$temp = $this->income_model->get_join('income_category');

		for($i=0; $i<count($temp); $i++) {
			$temp[$i]['type'] = "income";
		}

		$income_query = $temp;


		$this->load->model('expense_model');
		// $data['filedata']['expense_query'] = $this->expense_model->get_join('expense_category');	
		$temp = $this->expense_model->get_join('expense_category');	

		for($i=0; $i<count($temp); $i++) {
			$temp[$i]['type'] = "expense";
		}

		$expense_query = $temp;

		$query = array_merge($income_query, $expense_query);


		//selection sort of income and expenses ordered by ascending date
		for($i=0; $i<count($query)-1; $i++) {
			$min = $i;
			for ($j=$i+1; $j<count($query); $j++) {
				if ($query[$j]['date'] < $query[$min]['date'])
					$min = $j;
			}
			
			if ($min != $i) {
				$temp = $query[$i];
				$query[$i] = $query[$min];
				$query[$min] = $temp;
			}
		}

		$data['filedata']['query'] = $query;

		// echo "<pre>";
		// print_r($query);
		// exit;

		$this->load->view('includes/header');
		$this->load->view('includes/nav');
		$this->load->view('index', $data);
		$this->load->view('includes/footer');
	}
}