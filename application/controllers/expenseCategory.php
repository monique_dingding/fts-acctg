<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ExpenseCategory extends CI_Controller {

	function index() {
		redirect('home');
	}

	function get_post_data() {
		$data = array(
			'name' => $this->input->post('name', TRUE),
			'description' => $this->input->post('description', TRUE),
			'client_name' => $this->input->post('client_name', TRUE),
		);

		return $data;
	}

	function add() {
		$data['filename'] = "category/expense_add";

		$this->load->model('expenseCategory_model');
		$data['filedata']['categories'] = $this->expenseCategory_model->get();
		$data['filedata']['items'] = $this->session->flashdata('items');
		$data['filedata']['data'] = $this->get_post_data();

		// echo "<pre>";
		// print_r($data['filedata']); exit;

		$this->load->view('includes/header');
		$this->load->view('includes/nav');
		$this->load->view('index', $data);
		$this->load->view('includes/footer');
	}


	function insert() {
		$submit = $this->input->post('submit', TRUE);
		if ($submit == "Go") {

			$items = $this->input->post('items');
			$this->session->set_flashdata('items', $items);

			redirect('expenseCategory/add');
		} else if ($submit == "Submit") {
			$data = $this->get_post_data();

			$this->load->model('expenseCategory_model');
			$this->expenseCategory_model->insert($data);

			$message = "Item is successfully added.";
			$this->session->set_flashdata('message', $message);

			redirect('expense/categories');
		}
	}

	function update() {
		$id = $this->uri->segment(3);

		$data['filename'] = "category/expense_update";

		$this->load->model('expenseCategory_model');
		$query = $this->expenseCategory_model->get_where($id);
		$data['filedata']['data'] = $query[0];
		
		
		// echo "<pre>";
		// print_r($data['filedata']['data']); exit;

		$this->load->view('includes/header');
		$this->load->view('includes/nav');
		$this->load->view('index', $data);
		$this->load->view('includes/footer');
	}

	function update_cat() {
		$id = $this->input->post('id', TRUE);
		$data = $this->get_post_data();
		$submit = $this->input->post('submit', TRUE);

		if($submit == TRUE) {
			$this->load->model('expenseCategory_model');
			$this->expenseCategory_model->update($id, $data);

			$message = "Item is successfully updated.";
			$this->session->set_flashdata('message', $message);

			redirect('expense/categories');
		}

	}

	function delete() {

		$id = $this->uri->segment(3);
		
		$this->load->model('expenseCategory_model');
		$this->expenseCategory_model->delete($id);

		$message = "Item is successfully removed from the categories list.";
		$this->session->set_flashdata('message', $message);

		redirect('expense/categories');
	}
}