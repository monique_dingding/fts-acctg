<?php

class Expense_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function insert($data) {
    	$this->db->insert('expense', $data);
    }

    function get_join($tablename) {
    	$join_ids = "expense.expense_category_id = ".$tablename.".id";

    	$this->db->select('*');
    	$this->db->from($tablename);
    	$this->db->join('expense', $join_ids);
    	$this->db->order_by('date', 'asc');
    	$query = $this->db->get();

    	return $query->result_array();
    }

    function delete($id) {
        $this->db->where('id', $id);
        $this->db->delete('expense');
    }

    function get_where($id) {
        $this->db->select('*');
        $this->db->from('expense');
        $this->db->where('id', $id);
        $this->db->limit(1);
        $query = $this->db->get();

        return $query->result_array();
    }

    function update($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('expense', $data);
    }
}