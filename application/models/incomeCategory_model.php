<?php

class IncomeCategory_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get() {
        $this->db->select('*');
        $this->db->from('income_category');
        $query = $this->db->get();

        return $query->result_array();
    }

    function insert($data) {
    	$this->db->insert('income_category', $data);
    }

    function get_where($id) {
        $this->db->select('*');
        $this->db->from('income_category');
        $this->db->where('id', $id);
        $query = $this->db->get();

        return $query->result_array();
    }

    function update($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('income_category', $data);
    }

    function delete($id) {
        $this->db->where('id', $id);
        $this->db->delete('income_category');
    }
}