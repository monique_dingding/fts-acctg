<?php

class ExpenseCategory_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get() {
        $this->db->select('*');
        $this->db->from('expense_category');
        $query = $this->db->get();

        return $query->result_array();
    }

    function insert($data) {
    	$this->db->insert('expense_category', $data);
    }

    function get_where($id) {
        $this->db->select('*');
        $this->db->from('expense_category');
        $this->db->where('id', $id);
        $query = $this->db->get();

        return $query->result_array();
    }

    function update($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('expense_category', $data);
    }

    function delete($id) {
        $this->db->where('id', $id);
        $this->db->delete('expense_category');
    }
}