<?php

class Income_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function insert($data) {
    	$this->db->insert('income', $data);
    }

    function get_join($tablename) {
    	$join_ids = "income.income_category_id = ".$tablename.".id";

    	$this->db->select('*');
    	$this->db->from($tablename);
    	$this->db->join('income', $join_ids);
    	$this->db->order_by('date', 'asc');
    	$query = $this->db->get();

    	return $query->result_array();
    }

    function get_where($id) {
        $this->db->select('*');
        $this->db->from('income');
        $this->db->where('id', $id);
        $this->db->limit(1);
        $query = $this->db->get();

        return $query->result_array();
    }

    function delete($id) {
        $this->db->where('id', $id);
        $this->db->delete('income');
    }

    function update($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('income', $data);
    }
}