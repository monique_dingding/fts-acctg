<style type="text/css">
    .small-font {
        font-size: 12px;
    }

    #cancelStyle {
        top: 0px;
        right: 0px;
        position: relative;
        font-size: 14px;
        padding-top: 0.625em;
        padding-bottom: 0.625em;
        color: #777;
    }
</style>

<div class="large-9 columns">
    <h3><?php echo $page; ?> List</h3>

    <?php 
        $message = $this->session->flashdata('message');
    ?>
    <?php if (!empty($message)): ?>
    <div data-alert class="alert-box" style="background: #24C655; border-color: #059F33; padding-top: 0.7em">
        <a href="#" class="close">&times;</a>
        <span><?php echo $message; ?></span>                    
    </div>
    <?php endif; ?>

    <table width="100%">
        <thead>
            <tr>
            <th width="10">#</th>
            <th>Name</th>
            <th>Description</th>
            <?php 
                if ($page == "Income") {
                    echo "<th>Client Name</th>";
                } else {
                    echo "<th>Purchased By</th>";
                }
            ?>
            
            <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <?php $count = 0; ?>
            <?php foreach($categories as $c): ?>
            <tr>
            <td class="small-font"><?php echo ++$count; ?></td>
            <td class="small-font"><?php echo $c['name']; ?></td>
            <td class="small-font"><?php echo word_limiter($c['description'], 5); ?></td>
            <td class="small-font"><?php echo $c['client_name']; ?></td>
            <td class="small-font">
                <center>
                <?php
                if (!empty($page) && $page == "Income") {
                    $site = site_url()."/incomeCategory/update/".$c['id'];
                } else {
                    $site = site_url()."/expenseCategory/update/".$c['id'];
                }
                ?>
                <a href="<?php echo $site; ?>"><i class="fa fa-pencil-square-o"></i></a>
                <a href="#" style="color: #D80000" class="deleteLink" data-reveal-id="myModal" data-id="<?php echo $c['id']; ?>" data-type="<?php echo $page; ?>" data-name="<?php echo $c['name']; ?>"><?php echo nbs(2); ?><i class="fa fa-times"></i></a>
                </center>
            </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>

<div id="myModal" class="reveal-modal medium">
<form method="post" action="<?php echo site_url(); ?>/home/remove_category">
    <h3 class="titleModal"></h3><hr>
    <p class="sub"></p>
    <input type="hidden" class="item_id" name="id" value="">
    <input type="hidden" class="item_type" name="type" value="">
    <span class="pull-right"><input type="submit" class="small button" name="submit" value="Proceed"><?php echo nbs(4); ?>
    <a href="#" class="close-reveal-modal tiny button secondary" id="cancelStyle">Cancel</a></span>
    
    <!-- <a class="close-reveal-modal">&#215;</a> -->
</form>
</div>

<script type="text/javascript">

$("body").on( "click", ".deleteLink", function() {
    
    $('.titleModal').html("<span style='color: #D80000'>Delete " + $(this).attr('data-type') + " Category</span>");
    $('.sub').html("Remove the category <b>" + $(this).attr('data-name') + "</b> from the list? This action cannot be undone.");
    $('.item_id').val($(this).attr('data-id'));
    $('.item_type').val($(this).attr('data-type'));

});

</script>