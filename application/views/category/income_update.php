<div class="large-9 columns">
    <h3>Update Income Category</h3>
    <form method="post" action="<?php echo site_url(); ?>/incomeCategory/update_cat">
        <div class="small-10">
            <div class="row">
                <div class="small-4 columns">
                    <label for="right-label" class="right inline">Name</label>
                </div>
                <div class="small-8 columns">
                    <input type="text" id="right-label" placeholder="" name="name" value="<?php echo $data['name']; ?>" required>
                </div>
            </div>
            <div class="row">
                <div class="small-4 columns">
                    <label for="right-label1" class="right inline">Description</label>
                </div>
                <div class="small-8 columns">
                    <textarea type="text" id="right-label1" placeholder="" name="description" required><?php echo $data['description']; ?></textarea>
                </div>
            </div>
            <div class="row">
                <div class="small-4 columns">
                    <label for="right-label2" class="right inline">Client Name</label>
                </div>
                <div class="small-8 columns">
                    <input type="text" id="right-label2" placeholder="" name="client_name" value="<?php echo $data['client_name']; ?>" required>
                </div>
            </div>
            <div class="row">
                <div class="small-4 columns">
                    <label for="right-label" class="right inline"></label>
                </div>
                <div class="small-8 columns">
                    <input type="hidden" name="id" value="<?php echo $data['id']; ?>">
                    <input type="submit" style="margin-left: 18em" name="submit" class="small button " value="Submit">
                    <a href="<?php echo site_url(); ?>/income/categories" class="small button secondary">Cancel</a>
                </div>
            </div>
        </div>
    </form>
</div>