<div class="large-9 columns">
    <h3>Add Expense Category</h3>
    <form method="post" action="<?php echo site_url(); ?>/expenseCategory/insert">
        <div class="small-10">
            <div class="row">
                <div class="small-4 columns">
                    <label for="right-label" class="right inline">Name</label>
                </div>
                <div class="small-8 columns">
                    <input type="text" id="right-label" placeholder="" name="name" value="<?php echo $data['name']; ?>" required>
                </div>
            </div>
            <div class="row">
                <div class="small-4 columns">
                    <label for="right-label" class="right inline">Description</label>
                </div>
                <div class="small-8 columns">
                    <textarea type="text" id="right-label" placeholder="" name="description" value="<?php echo $data['description']; ?>" required></textarea>
                </div>
            </div>
            <div class="row">
                <div class="small-4 columns">
                    <label for="right-label" class="right inline">Purchased By</label>
                </div>
                <div class="small-8 columns">
                    <input type="text" id="right-label" placeholder="" name="client_name" value="<?php echo $data['client_name']; ?>" required>
                </div>
            </div>
            <?php if (empty($items)): ?>
            <div class="row">
                <div class="small-4 columns">
                    <label for="right-label" class="right inline"></label>
                </div>
                <div class="small-8 columns">
                    <input type="submit" name="submit" href="#" class="small button pull-right" value="Submit">
                </div>
            </div>
            <div class="row">
                <div class="small-4 columns">
                    <label for="right-label" class="right inline"></label>
                </div>
                <div class="small-8 columns">
                    <p class="pull-right" style="display: inline;"><?php echo nbs(4); ?><span style="font-size: 12px">or add <?php echo nbs(2); ?><input type="text" id="right-label" style="width: 3em; display: inline" name="items"> item/s </span><?php echo nbs(2); ?> <input type="submit" href="#" class="tiny button" name="submit" value="Go"></p>
                </div>
            </div>
            <?php endif; ?>
        </div>
        <?php if (!empty($items)): ?>
            <input type="hidden" name="items" value="<?php echo $items; ?>">
            <?php while($items>0) { ?>
            <hr>
            <div class="small-10">
                <div class="row">
                    <div class="small-4 columns">
                        <label for="right-label" class="right inline">Name</label>
                    </div>
                    <div class="small-8 columns">
                        <input type="text" id="right-label" placeholder="" name="name_<?php echo $items; ?>" required>
                    </div>
                </div>
                <div class="row">
                    <div class="small-4 columns">
                        <label for="right-label" class="right inline">Description</label>
                    </div>
                    <div class="small-8 columns">
                        <textarea type="text" id="right-label" placeholder="" name="description_<?php echo $items; ?>" required></textarea>
                    </div>
                </div>
                <div class="row">
                    <div class="small-4 columns">
                        <label for="right-label" class="right inline">Purchased By</label>
                    </div>
                    <div class="small-8 columns">
                        <input type="text" id="right-label" placeholder="" name="client_name_<?php echo $items; ?>" required>
                    </div>
                </div>
                <?php if ($items == 1): ?>
                <div class="row">
                    <div class="small-4 columns">
                        <label for="right-label" class="right inline"></label>
                    </div>
                    <div class="small-8 columns">
                        <input type="submit" name="submit" href="#" class="small button pull-right" value="Submit">
                    </div>
                </div>
<!--                 <div class="row">
                    <div class="small-4 columns">
                        <label for="right-label" class="right inline"></label>
                    </div>
                    <div class="small-8 columns">
                        <p class="pull-right" style="display: inline;"><?php echo nbs(4); ?><span style="font-size: 12px">or add <?php echo nbs(2); ?><input type="text" id="right-label" style="width: 3em; display: inline" name="items"> item/s </span><?php echo nbs(2); ?> <input type="submit" href="#" class="tiny button" name="submit" value="Go"></p>
                    </div>
                </div> -->
                <?php endif; ?>
            </div>
            <?php $items--; ?>
            <?php } ?>
        <?php endif; ?>
    </form>
</div>