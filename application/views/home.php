<style type="text/css">
	.small-font {
		font-size: 12px;
	}

	#cancelStyle {
		top: 0px;
		right: 0px;
		position: relative;
		font-size: 14px;
		padding-top: 0.625em;
		padding-bottom: 0.625em;
		color: #777;
	}
</style>


<div class="large-10 columns">
	<h3>Finance Summary</h3>
	<div class="section-container tabs" data-section>
		<section class="section <?php if ($active == "income") echo "active"; ?>">
			<h5 class="title"><a href="#panel1">Income</a></h5>
			<div class="content" data-slug="panel1">
				<?php 
					$message = $this->session->flashdata('message');
				?>
				<?php if (!empty($message)): ?>
				<div data-alert class="alert-box" style="background: #24C655; border-color: #059F33; padding-top: 0.7em">
					<a href="#" class="close">&times;</a>
					<span><?php echo $message; ?></span>					
				</div>
				<?php endif; ?>
				<?php
					$total = 0;
					foreach($income_query as $q) {
						$total += $q['amount'];
					}
				?>
				<p style="text-align: right"><b>Total: P<?php echo number_format($total, 2); ?> </b></p>
				<table width="100%">
					<thead>
					<tr>
					<th width="10">#</th>
					<th>Rcpt #</th>
					<th>Date</th>
					<th>Category</th>
					<th>Description</th>
					<th>Amount</th>
					<th>Action</th>
					</tr>
					</thead>
					<tbody>
					<?php $count = 0; ?>
					<?php foreach($income_query as $q): ?>
					<tr>
					<td class="small-font"><?php echo ++$count; ?></td>
					<td class="small-font"><?php  echo $q['receipt_number']; ?></td>
					<td class="small-font"><?php $timestamp = strtotime($q['date']); echo date('M d, Y', $timestamp); ?></td>
					<td class="small-font"><?php echo $q['name']; ?></td>
					<td class="small-font"><?php echo word_limiter($q['description'], 5); ?></td>
					<td class="small-font">₱ <?php echo $q['amount']; ?></td>
					<td>
						<center>
						<a href="<?php echo site_url(); ?>/income/update/<?php echo $q['id']; ?>"><i class="fa fa-pencil-square-o"></i></a> 
						<a href="#" data-reveal-id="myModal" style="color: #D80000" class="deleteLink" data-id="<?php echo $q['id']; ?>" data-description="<?php echo $q['description']; ?>" data-receipt="<?php echo $q['receipt_number']; ?>" data-type="Income"><i class="fa fa-times"></i></a>
						</center>
					</td>
					</tr>
					<?php endforeach; ?>
					</tbody>
				</table>
			</div>
		</section>
		<section class="section <?php if ($active == "expense") echo "active"; ?>">
			<h5 class="title"><a href="#panel2">Expense</a></h5>
			<div class="content" data-slug="panel2">
				<?php 
					$message = $this->session->flashdata('message');
				?>
				<?php if (!empty($message)): ?>
				<div data-alert class="alert-box" style="background: #24C655; border-color: #059F33; padding-top: 0.7em">
					<a href="#" class="close">&times;</a>
					<span><?php echo $message; ?></span>					
				</div>
				<?php endif; ?>
				<?php
					$total = 0;
					foreach($expense_query as $q) {
						$total += $q['amount'];
					}
				?>
				<p style="text-align: right"><b>Total: P<?php echo number_format($total, 2); ?> </b></p>
				<table width="100%">
					<thead>
					<tr>
					<th width="10">#</th>
					<th>Rcpt #</th>
					<th>Date</th>
					<th>Category</th>
					<th>Description</th>
					<th>Amount</th>
					<th>Action</th>
					</tr>
					</thead>
					<tbody>
					<?php $count = 0; ?>
					<?php foreach($expense_query as $q): ?>
					<tr>
					<td class="small-font"><?php echo ++$count; ?></td>
					<td class="small-font"><?php echo $q['receipt_number']; ?></td>
					<td class="small-font"><?php $timestamp = strtotime($q['date']); echo date('M d, Y', $timestamp); ?></td>
					<td class="small-font"><?php echo $q['name']; ?></td>
					<td class="small-font"><?php echo word_limiter($q['description'], 5); ?></td>
					<td class="small-font">₱ <?php echo $q['amount']; ?></td>
					<td>
						<center>
						<a href="<?php echo site_url(); ?>/expense/update/<?php echo $q['id']; ?>"><i class="fa fa-pencil-square-o"></i></a> 
						<a href="#" data-reveal-id="myModal" style="color: #D80000" class="deleteLink" data-id="<?php echo $q['id']; ?>" data-description="<?php echo $q['description']; ?>" data-receipt="<?php echo $q['receipt_number']; ?>" data-type="Expense"><i class="fa fa-times"></i></a>
						</center>
					</td>
					</tr>
					<?php endforeach; ?>
					</tbody>
				</table>
			</div>
		</section>
	</div>
</div>

<div id="myModal" class="reveal-modal medium">
<form method="post" action="<?php echo site_url(); ?>/home/remove_item">
	<h3 class="titleModal"></h3><hr>
	<p class="sub"></p>
	<input type="hidden" class="item_id" name="id" value="">
	<input type="hidden" class="item_type" name="type" value="">
	<span class="pull-right"><input type="submit" class="small button" name="submit" value="Proceed"><?php echo nbs(4); ?>
	<a href="#" class="close-reveal-modal tiny button secondary" id="cancelStyle">Cancel</a></span>
	
	<!-- <a class="close-reveal-modal">&#215;</a> -->
</form>
</div>

<script type="text/javascript">

$("body").on( "click", ".deleteLink", function() {
	
    $('.titleModal').html("<span style='color: #D80000'>Delete " + $(this).attr('data-type') + " Item</span>");
    $('.sub').html("Remove <b>#" + $(this).attr('data-receipt') + " " + $(this).attr('data-description') + "</b> from the list? This action cannot be undone.");
    $('.item_id').val($(this).attr('data-id'));
    $('.item_type').val($(this).attr('data-type'));

});

</script>