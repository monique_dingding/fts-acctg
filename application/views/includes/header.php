<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>FTS Accounting</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assets/css/normalize.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assets/css/foundation4.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
</head>

<script src="<?php echo base_url(); ?>/assets/js/vendor/jquery.js"></script>
<script src="<?php echo base_url(); ?>/assets/js/vendor/foundation4.js"></script>
<body>