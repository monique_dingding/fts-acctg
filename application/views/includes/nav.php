<nav class="top-bar nav-color">
	<ul class="title-area">
		<li class="name">
		<h1>
		<a href="<?php echo site_url(); ?>">
		Futuristech Studios, Inc.
		</a>
		</h1>
		</li>
		<li class="toggle-topbar menu-icon"><a href="#"><span>menu</span></a></li>
	</ul>
	<section class="top-bar-section">
	 
	<ul class="right">
		<li class="divider"></li>
		<li><a href="<?php echo site_url(); ?>"><i class="fa fa-home" style="font-size: 17px;"></i> Home</a></li>
		<li class="divider"></li>
		<li><a href="<?php echo site_url(); ?>/home/general_ledger"><i class="fa fa-calculator"></i> Ledger</a></li>
		<li class="divider"></li>
		<li class="has-dropdown">
			<a href="#"><i class="fa fa-tasks"></i> Categories</a>
			<ul class="dropdown">
				<li><a href="<?php echo site_url(); ?>/income/categories">Income</a></li>
				<li><a href="<?php echo site_url(); ?>/expense/categories">Expense</a></li>
			</ul>
		</li>
	</ul>
	</section>
</nav>