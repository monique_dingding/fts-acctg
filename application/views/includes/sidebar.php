<style type="text/css">
	.move-left {
		text-align: left;
	}
</style>

<div class="large-2 columns">
	<h4>Actions</h4>
	<ul class="pricing-table">
		<!-- <li class="description" style="text-align: left"><a href="#"><i class="fa fa-folder-open"></i><?php echo nbs(3); ?>View General Ledger</a></li>
		<li class="description" style="text-align: left"><a href="#"><i class="fa fa-folder-open"></i><?php echo nbs(3); ?>View Categories</a></li>
		 -->
		<li class="bullet-item" style="text-align: left">Income</li>
		<li class="description" style="text-align: left"><a href="<?php echo site_url(); ?>/income/add"><i class="fa fa-plus"></i><?php echo nbs(3); ?>Add Item</a></li>
		<li class="description" style="text-align: left"><a href="<?php echo site_url(); ?>/incomeCategory/add"><i class="fa fa-plus"></i><?php echo nbs(3); ?>Add Category</a></li>
		<!-- <li class="description" style="text-align: left"><a href="<?php echo site_url(); ?>/incomeCategory/add" style="color: #D80000"><i class="fa fa-times"></i><?php echo nbs(3); ?>Remove Multiple Items</a></li> -->
		
		<li class="bullet-item" style="text-align: left">Expense</li>
		<li class="description" style="text-align: left"><a href="<?php echo site_url(); ?>/expense/add"><i class="fa fa-plus"></i><?php echo nbs(3); ?>Add Item</a></li>
		<li class="description" style="text-align: left"><a href="<?php echo site_url(); ?>/expenseCategory/add"><i class="fa fa-plus"></i><?php echo nbs(3); ?>Add Category</a></li>
		<!-- <li class="description" style="text-align: left"><a href="<?php echo site_url(); ?>/incomeCategory/add" style="color: #D80000"><i class="fa fa-times"></i><?php echo nbs(3); ?>Remove Multiple Items</a></li> -->

	</ul>
</div>