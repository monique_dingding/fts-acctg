<div class="large-9 columns">
    <h3>Update Income Item</h3>
    <form method="post" action="<?php echo site_url(); ?>/income/update_item">
        <div class="small-10">
            <div class="row">
                <div class="small-4 columns">
                    <label for="right-label" class="right inline">Category</label>
                </div>
                <div class="small-8 columns">
                    <select class="small" style="padding-top: 7px; padding-bottom: 7px; font-size: 13px" name="income_category_id">
                        <?php foreach($categories as $c): ?>
                            <?php if ($query['income_category_id'] == $c['id']): ?>
                                <option value="<?php echo $c['id'] ?>"><?php echo $c['name'] ?></option>
                            <?php endif; ?>
                        <?php endforeach; ?>

                        <?php foreach($categories as $c): ?>
                            <?php if ($query['income_category_id'] != $c['id']): ?>
                                <option value="<?php echo $c['id'] ?>"><?php echo $c['name'] ?></option>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="small-4 columns">
                    <label for="right-label" class="right inline">Receipt Number</label>
                </div>
                <div class="small-8 columns">
                    <input type="text" id="right-label" placeholder="" name="receipt_number" value="<?php echo $query['receipt_number']; ?>">
                </div>
            </div>
            <div class="row">
                <div class="small-4 columns">
                    <label for="right-label" class="right inline">Date Issued</label>
                </div>
                <div class="small-8 columns">
                    <input type="text" id="right-label" placeholder="MM/DD/YYYY" name="date" value="<?php echo $query['date']; ?>">
                </div>
            </div>
            <div class="row">
                <div class="small-4 columns">
                    <label for="right-label" class="right inline">Description</label>
                </div>
                <div class="small-8 columns">
                    <textarea type="text" id="right-label" placeholder="" name="description"><?php echo $query['description']; ?></textarea>
                </div>
            </div>
            <div class="row">
                <div class="small-4 columns">
                    <label for="right-label" class="right inline">Amount</label>
                </div>
                <div class="small-8 columns">
                    <input type="text" id="right-label" placeholder="PHP " name="amount" value="<?php echo $query['amount']; ?>">
                </div>
            </div>
            <div class="row">
                <div class="small-4 columns">
                    <label for="right-label" class="right inline"></label>
                </div>
                <div class="small-8 columns">
                    <input type="hidden" name="id" value="<?php echo $query['id']; ?>">
                    <input type="submit" style="margin-left: 18em" name="submit" class="small button " value="Submit">
                    <a href="<?php echo site_url(); ?>" class="small button secondary">Cancel</a>
                </div>
            </div>
        </div>
    </form>
</div>