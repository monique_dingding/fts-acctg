<div class="row" style="margin-top: 2em">
	<?php

	if (!isset($filedata)) {
		$this->load->view($filename);
	} else {
		$this->load->view($filename, $filedata);
	}

	$this->load->view('includes/sidebar');

	?>
	 
</div>
	 
	 
<footer class="row">
	<div class="large-12 columns">
		<hr/>
		<div class="row">
		<div class="large-6 columns">
		<p>&copy; Futuristech Studios, Inc.</p>
		</div>
		<!-- <div class="large-6 columns">
		<ul class="inline-list right">
		<li><a href="#">Link 1</a></li>
		<li><a href="#">Link 2</a></li>
		<li><a href="#">Link 3</a></li>
		<li><a href="#">Link 4</a></li>
		</ul>
		</div> -->
		</div>
	</div>
</footer>
	 
	 
<div class="reveal-modal" id="mapModal">
	<h4>Where We Are</h4>
	<p><img src="http://placehold.it/800x600"/></p>
	 
	<a href="#" class="close-reveal-modal">&times;</a>
</div>