<div class="large-9 columns">
    <h3>Add Expense</h3>
    <form method="post" action="<?php echo site_url(); ?>/expense/insert">
        <div class="small-10">
            <div class="row">
                <div class="small-4 columns">
                    <label for="right-label" class="right inline">Category</label>
                </div>
                <div class="small-8 columns">
                    <select class="small" style="padding-top: 7px; padding-bottom: 7px; font-size: 13px" name="expense_category_id">
                        <?php foreach($categories as $c): ?>
                        <option value="<?php echo $c['id'] ?>"><?php echo $c['name'] ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="small-4 columns">
                    <label for="right-label" class="right inline">Receipt Number</label>
                </div>
                <div class="small-8 columns">
                    <input type="text" id="right-label" placeholder="" name="receipt_number">
                </div>
            </div>
            <div class="row">
                <div class="small-4 columns">
                    <label for="right-label" class="right inline">Date Issued</label>
                </div>
                <div class="small-8 columns">
                    <input type="text" id="right-label" placeholder="YYYY-MM-DD" name="date">
                </div>
            </div>
            <div class="row">
                <div class="small-4 columns">
                    <label for="right-label" class="right inline">Description</label>
                </div>
                <div class="small-8 columns">
                    <textarea type="text" id="right-label" placeholder="" name="description"></textarea>
                </div>
            </div>
            <div class="row">
                <div class="small-4 columns">
                    <label for="right-label" class="right inline">Amount</label>
                </div>
                <div class="small-8 columns">
                    <input type="text" id="right-label" placeholder="PHP " name="amount">
                </div>
            </div>
            <div class="row">
                <div class="small-4 columns">
                    <label for="right-label" class="right inline"></label>
                </div>
                <div class="small-8 columns">
                    <input type="submit" name="submit" href="#" class="small button pull-right" value="Submit">
                </div>
            </div>
            <div class="row">
                <div class="small-4 columns">
                    <label for="right-label" class="right inline"></label>
                </div>
                <div class="small-8 columns">
                    <p class="pull-right" style="display: inline;"><?php echo nbs(4); ?><span style="font-size: 12px">or add <?php echo nbs(2); ?><input type="text" id="right-label" style="width: 3em; display: inline" name="items"> item/s </span><?php echo nbs(2); ?> <input type="submit" href="#" class="tiny button" name="submit" value="Go"></p>
                </div>
            </div>
        </div>
    </form>
</div>