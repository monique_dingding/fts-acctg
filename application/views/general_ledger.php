<style type="text/css">
    .small-font {
        font-size: 12px;
    }
</style>

<div class="large-9 columns">
    <h3>Cash Ledger</h3>
    <table width="100%">
        <thead>
            <tr>
            <th width="5%">#</th>
            <th width="15%">Date</th>
            <th>Description</th>
            <th>Debit</th>
            <th>Credit</th>
            <th>Balance</th>
            </tr>
        </thead>
        <tbody>
            <?php
                $count = 1;
                $balance = 0;
                foreach($query as $q) {
                        $timestamp = strtotime($q['date']);
                        

                        echo "<tr>";
                        echo "<td class='small-font'>".$count."</td>";
                        echo "<td class='small-font'>".date('M d, Y', $timestamp)."</td>";
                        echo "<td class='small-font'>".$q['description']."</td>";
                        
                        if ($q['type'] == "income") {
                            $balance += $q['amount'];

                            echo "<td class='small-font'>₱ ".$q['amount']."</td>";
                            echo "<td class='small-font'> </td>";
                        } else if ($q['type'] == "expense") {
                            $balance -= $q['amount'];

                            echo "<td class='small-font'> </td>";
                            echo "<td class='small-font'>₱ ".$q['amount']."</td>";

                        }
                        
                        echo "<td class='small-font'>₱ ".number_format($balance, 2)."</td>";
                        echo "</tr>";
                        $count++;                        
                }
                echo "<tr><td></td><td></td><td></td><td></td><td></td><td></td></tr>";
                echo "<tr><td class='small-font'><b>Total</b></td><td></td><td></td><td></td><td></td><td class='small-font'><b>₱ ".$balance."</b></td></tr>";

            ?>
        </tbody>
    </table>
</div>